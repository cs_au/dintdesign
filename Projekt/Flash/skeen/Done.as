﻿package skeen
{
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.URLLoader;
	import flash.net.URLRequestMethod;
	import flash.net.URLLoaderDataFormat;

	public class Done extends MovieClip
	{
		private var stagy:Stage = null;

		private var clickCounter:Number = 0;
		private var clickString:String = "";

		public function Done(stage_init:Stage)
		{
			stagy = stage_init;
			stagy.addEventListener(MouseEvent.MOUSE_DOWN, clickHandler);
			addEventListener(MouseEvent.CLICK, sendDataEvent);
		}

		private function sendDataEvent(e:MouseEvent):void
		{
			var requesty:URLRequest = new URLRequest("http://duck.myplus.org/dIntDesign/array_test.php");
			requesty.method = URLRequestMethod.POST;

			var variables:URLVariables = new URLVariables();

			variables.datastring = clickString;
			requesty.data = variables;

			var loader:URLLoader = new URLLoader();
			loader.dataFormat = URLLoaderDataFormat.VARIABLES;
			loader.addEventListener(Event.COMPLETE, doComplete);
			loader.load(requesty);

			clickCounter = 0;
			clickString = "";
		}

		private function doComplete(event:Event):void
		{
			trace(event.target.data);
		}

		private function clickHandler(e:MouseEvent):void
		{
			if(e.target.name == null)
				return;
			clickCounter++;

			if (e.target.parent.parent.name == "textfield")
			{
				clickString +=  "|[" + clickCounter + "] " + e.target.parent.parent.text;
			}
			else if (e.target.name == "icon")
			{
				clickString +=  "|[" + clickCounter + "] " + e.target.parent.getChildAt(1).text;
			}
			else
			{
				clickString +=  "|[" + clickCounter + "] " + e.target.name;
			}
			trace(clickString);
		}


	}
}