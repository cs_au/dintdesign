﻿package skeen
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.IOErrorEvent;
	
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.display.Sprite;
	import flash.display.BitmapData;
	import flash.geom.Matrix;

	public class listItem extends MovieClip
	{
		// Used for calculating mouse movement distance
		private var startY:Number = 0;

		// Used to load the icons
		private var loader:Loader = new Loader();

		public function listItem(icon:String)
		{
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, renderImg);
            loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			loader.load(new URLRequest(icon));
			
			this.addEventListener(MouseEvent.MOUSE_UP, itemMouseUp);
			this.addEventListener(MouseEvent.MOUSE_DOWN, itemMouseDown);
		}

		public function Fade()
		{
			gotoAndStop(2);
		}

		public function deFade()
		{
			gotoAndStop(1);
		}
		
		// Used to render the image
		private function renderImg(event:Event):void
		{
            var myBitmap:BitmapData = new BitmapData(loader.width, loader.height, false);
			var scaletrix:Matrix = new Matrix();
			scaletrix.scale(1.0/6, 1.0/6);
			
			myBitmap.draw(loader, scaletrix);
            icon.graphics.beginBitmapFill(myBitmap, new Matrix(), true);
            icon.graphics.drawRect(0, 0, 50, 50);
            icon.graphics.endFill();
		}

		// Error catching listener
		private function ioErrorHandler(e:IOErrorEvent):void
		{
            trace("Unable to load icon image!");
        }
		/* 
		 * Mouse down event.
		 * Calculates the startY position on the grand parent.
		 * (ListItem's parent is ListContainer)
		 * (ListContainer's parent is List)
		 * It is possible that this method could be changed to easily
		 * utilize the stage instead (stage.mouseY). 
		 */
		function itemMouseDown(e:MouseEvent):void
		{
			startY = parent.parent.mouseY;
		}

		/* 
		 * Mouse up event.
		 * Calculates the distance the mouse has traveled using the
		 * current location (parent.parent.mouseY) and the startY
		 * variable. 8 is the threshold.
		 * The trace should be replaced, possibly by a call to the
		 * stage or another movieclip to act accordingly (bring up
		 * a new list, for instance).
		 */
		function itemMouseUp(e:MouseEvent):void
		{
			if (Math.abs(startY - parent.parent.mouseY) < 2)
			{
				// Push this event, to be catched higher up
				dispatchEvent(new Event(Event.SELECT));
			}
		}

	}
}