﻿package skeen
{
	public class xml_util
	{
		public function xml_util()
		{
			trace("DONT FUCKING INSTANCE ME YOU NOOB!");
		}
		
		/*
 		 * Takes a search string specifying what we want to find, and an XMLList in which we search
 		 * Returns a list of elements.
 		 */
		public static function search(str:String, data:XMLList):XMLList
		{
			var xmll:XMLList = new XMLList();
			
			for (var j:Number = 0; j<data.length(); j++)
			{
				if(data[j].name.toLowerCase().search(str.toLowerCase())!=-1)
				{
					xmll += XML(data[j]);
					continue;
				}
				else
				{
					for (var i:Number = 0; i<data[j].category.length(); i++)
					{
						if(data[j].category[i].toLowerCase().search(str.toLowerCase())!=-1)
						{
							xmll += XML(data[j]);
							break;
						}
					}
				}
			}
			return xmll;
		}
		
		/*
 		 * Takes a string specifying the element we want children from.
		 * Tages an XMLList to search from.
 		 * Returns an XMLList, of the child elements.
 		 */
		public static function searchChildren(str:String, data:XMLList):XMLList
		{
			for(var j:Number = 0; j<data.length(); j++)
			{
				if(data[j].name==str)
				{
					return (data[j].children());
				}
			}
			return null;
		}
		
		/*
 		 * Takes a string specifying the catagory we want elements from.
 		 * And an xml to search for it in, and then it simply searches and adds as it goes.
 		 * Returns a list of elements, containing the catagory string, in the xml.
 		 */
		public static function searchCatagory(catagory:String, data:XMLList):XMLList
		{
			var xmll:XMLList = new XMLList();
			for (var j:Number = 0; j<data.length(); j++)
			{
				for (var i:Number = 0; i<data[j].category.length(); i++)
				{
					if(catagory.toLowerCase()==data[j].category[i].toLowerCase())
					{
						xmll += XML(data[j]);
						break;
					}
				}
			}
			return xmll;
		}
	}
}
