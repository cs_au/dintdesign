﻿package skeen
{
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.geom.Rectangle;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import skeen.listItem;
	import flash.display.DisplayObjectContainer;

	public class list extends MovieClip
	{
		// Used to hold the dataz
		private var text_array:Array = [];
		private var data_array:Array = [];
		private var selected_index:int = -1;
		
		private var mask_height:uint = 250;
		
		// Used for adding eventhandling
		private var engineCallback:Function;

		public function list()
		{
			// Add eventlisteners.
			addEventListener(MouseEvent.MOUSE_DOWN, listMouseDown);
			addEventListener(MouseEvent.MOUSE_UP, listMouseUp);
			addEventListener(MouseEvent.MOUSE_OUT, listMouseOut);
			addEventListener(MouseEvent.MOUSE_MOVE, listMouseMove);
		}
		
		public function seedCallback(func:Function):void
		{
			engineCallback = func;
		}

		/*
		 * Add the given string+Object to the scene
		 */
		public function add(text:String, data:XML, icon:String):void
		{
			var newlistItem:listItem = new listItem(icon);
			newlistItem.y = text_array.length*50;
			newlistItem.textfield.text = text;
			newlistItem.addEventListener(Event.SELECT, engineCallback);
			container.addChild(newlistItem);

			text_array.push(newlistItem);
			data_array.push(data);
			
			container.y=0;
		}

		public function setHeight(new_height:uint):void
		{
			var old:Number = height;
			height = new_height;
			container.scaleY = container.scaleY*(old/height);
			scrollbar.scaleY = scrollbar.scaleY*(old/height);
			mask_height = mask_height*(old/height);
		}

		/* 
		 * Empties out the list, removing everything from the scene
		 */
		public function removeAll():void
		{
			for (var j:Number = 0; j<text_array.length; j++)
			{
				text_array[j].removeEventListener(Event.SELECT, engineCallback);
			}
			while(container.numChildren>0)
			{
				container.removeChildAt(0);
			}
			while (text_array.length!=0)
			{
				text_array.pop();
			}
			while (data_array.length!=0)
			{
				data_array.pop();
			}
		}
		
		public function deselectAll():void
		{
			for (var j:Number = 0; j<text_array.length; j++)
			{
				text_array[j].deFade();
			}
		}
		
		public function getCurrentlySelected():XML
		{
			if(selected_index!=-1)
			{
				return data_array[selected_index];
			}
			return null;
		}
		
		public function fadeSelected(select:listItem):Boolean
		{
			deselectAll();
			selected_index = text_array.indexOf(select);
			if(selected_index!=-1)
			{
				text_array[selected_index].Fade();
				return true;
			}
			return false;
		}

		/*
		 * Returns the current size of the list
		 */
		private function size():uint
		{
			return text_array.length;
		}

		/* 
		 * Mouse down event.
		 * list.container.startDrag allows dragging of the container
		 * object inside of the List object. The dragging is bounded by
		 * the "new Rectangle()" to lock it horizontally and not allow
		 * scrolling beyond the actual list.
		 */
		private function listMouseDown(e:MouseEvent):void
		{
			trace(mask_height);
			container.startDrag(false, new Rectangle(0,-(height-mask_height),0,height-mask_height));
			scrollbar.gotoAndPlay(2);
		}

		/* 
		 * Mouseup event.
		 * Stops the dragging procedure.
		 */
		private function listMouseUp(e:MouseEvent):void
		{
			container.stopDrag();
			scrollbar.gotoAndPlay(20);
		}

		/* 
		 * Mouse out event.
		 * Fixes a bug when dragging continues after mouse button release
		 * if the cursor is outside of the list object.
		 */
		private function listMouseOut(e:MouseEvent):void
		{
			if ((e.buttonDown) && (stage.mouseY < y || stage.mouseY > y+230))
			{
				scrollbar.gotoAndPlay(20);
				container.stopDrag();
			}
		}

		/* 
		 * Mouse move event.
		 * Used to update the vertical location of the scroll ball on the
		 * scollbar.
		 */
		private function listMouseMove(e:MouseEvent):void
		{
			if (e.buttonDown)
			{
				// The container's movement is sampled down to 0.0 - 1.0
				var percentage:Number = (container.y/(container.height-mask_height));
				// 10 is the radius of the ball, -230 is the height of the mask minus 20.
				scrollbar.scroll.ball.y = 10 + percentage*(-230);
			}
		}
	}
}