import os
import subprocess

rootdir='C:\Users\Skeen\Dropbox\HorsePower\dIntDesign\Projekt\Flash\gfx'
commands='-resize 400x400'

for subdir, dirs, files in os.walk(rootdir):
    for file in files:
        proc = subprocess.Popen("convert " + file + 
                                " " + commands + " "
                                + file, shell=True)
        proc.wait()
