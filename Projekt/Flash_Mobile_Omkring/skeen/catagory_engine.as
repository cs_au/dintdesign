﻿package skeen
{
	// Flash packages
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.system.fscommand;
	
	public class catagory_engine extends MovieClip
	{
		// The main list object
		private var cat_list:list = null;
		private var data_list:list = null;
		
		// Searchy
		private var searchy:searchBar = null;
		private var keyboardy:keyboard = null;
		
		// bottomy
		private var bottomy:bottomBar = null;
		
		// Google maps 
		private var mappy:map = null;
	
		// Loader stuff
		private var catagories_loader:URLLoader = null;
		private var catagories:XMLList = null;
		private var catagories_root:XMLList = null;
	
		private var sites_loader:URLLoader = null;
		private var sites:XMLList = null;
		
		// stageVar
		//private var stagy:Stage = null;
		private var stagelol:Stage = null;
		private var stagy:stager = null;
		
		private static var drawOffset_y:Number = 58;
	
		public function catagory_engine(stagevar_init:Stage, stagery:stager)
		{
			//stagy = stagevar_init;
			stagelol = stagevar_init;
			stagy = stagery;
			
			catagories_loader = new URLLoader();
			sites_loader = new URLLoader();
			
			// Load XML
			catagories_loader.addEventListener(Event.COMPLETE, function(e:Event)
			{	
				catagories_root = catagories = (new XML(catagories_loader.data)).children();
				fill_catlist(catagories, listItemSelected_CallBack);
			});
			sites_loader.addEventListener(Event.COMPLETE, function(e:Event)
			{	
				sites = (new XML(sites_loader.data)).children();	
			});
			catagories_loader.load(new URLRequest("catagories.xml"));
			sites_loader.load(new URLRequest("sites.xml"));
		}
		
		public function render():void
		{
			keyboardy = new keyboard();
			keyboardy.addEventListener(Event.ACTIVATE, keyboard_key_pressed);
			keyboardy.addEventListener(Event.UNLOAD, keyboard_enter_pressed);
			keyboardy.visible = false;
			stagy.addChild(keyboardy);
			
			searchy = new searchBar();
			searchy.textfield.addEventListener(MouseEvent.CLICK, search_field_clicked);
			stagy.addChild(searchy);
			
			bottomy = new bottomBar();
			bottomy.y = 640;
			bottomy.back.addEventListener(MouseEvent.MOUSE_DOWN, back_btn_down);
			bottomy.back.addEventListener(MouseEvent.MOUSE_UP, back_btn_up);
			bottomy.back.textfield.text = "Back";
			bottomy.home.addEventListener(MouseEvent.MOUSE_DOWN, home_btn_down);
			bottomy.home.addEventListener(MouseEvent.MOUSE_UP, home_btn_up);
			bottomy.home.textfield.text = "Home";
			stagy.addChild(bottomy);
			
			cat_list = new list();
			cat_list.y = drawOffset_y;
			stagy.addChild(cat_list);
			
			data_list = new list();
			data_list.y = 400-drawOffset_y;
			data_list.visible = false;
			stagy.addChild(data_list);
			
			mappy = new map();
			stagy.addChild(mappy);
		}
		
		private function destroy():void
		{
			stagy.removeChild(mappy);
			mappy = null;
			
			stagy.removeChild(data_list);
			data_list = null;
			
			stagy.removeChild(cat_list);
			cat_list = null;
			
			stagy.removeChild(bottomy);
			bottomy.home.removeEventListener(MouseEvent.MOUSE_UP, home_btn_up);
			bottomy.home.removeEventListener(MouseEvent.MOUSE_DOWN, home_btn_down);
			bottomy.back.removeEventListener(MouseEvent.MOUSE_UP, back_btn_up);
			bottomy.back.removeEventListener(MouseEvent.MOUSE_DOWN, back_btn_down);
			bottomy = null;
			
			stagy.removeChild(searchy);
			searchy.textfield.removeEventListener(MouseEvent.CLICK, search_field_clicked);
			searchy = null;
			
			stagy.removeChild(keyboardy);		
			keyboardy.addEventListener(Event.UNLOAD, keyboard_enter_pressed);
			keyboardy.addEventListener(Event.ACTIVATE, keyboard_key_pressed);
			keyboardy = null;
		}
		
		private function fill_catlist(data:XMLList, func:Function):void
		{
			cat_list.visible = true;
			data_list.visible = false;
			cat_list.seedCallback(func);
			cat_list.removeAll();
			for(var j:Number = 0; j<data.length(); j++)
			{
				if(skeen_utils.clearString(data[j].name) == "")
					continue;
				cat_list.add(data[j].name, data[j], data[j].icon);
			}
		}
		
		private function fill_datalist(data):void
		{
			cat_list.visible = false;
			data_list.visible = true;
			data_list.seedCallback(dataItemSelected_CallBack);
			data_list.removeAll();
			for(var j:Number = 0; j<data.length; j++)
			{
				data_list.add(data[j].name, data[j], data[j].icon);
			}
		}
		
		private function search_field_clicked(e:Event):void
		{
			keyboardy.visible = true;
			keyboardy.y = 375;
			if(searchy.textfield.text!="Search...")
				keyboardy.text = searchy.textfield.text;
		}
		
		private function keyboard_key_pressed(e:Event):void
		{
			searchy.textfield.text = e.currentTarget.text;
			
			var search_result:XMLList = xml_util.search(searchy.textfield.text, sites);
			fill_catlist(search_result, searchItemSelected_CallBack);
		}
		
		private function keyboard_enter_pressed(e:Event):void
		{
			keyboardy.visible = false;
			
			var search_result:XMLList = xml_util.search(searchy.textfield.text, sites);
			fill_datalist(search_result);
			
			mappy.seedNodes(search_result);
			if(!mappy.isRendering())
			{
				mappy.render();
			}
			
			//Change to search listener
			bottomy.back.removeEventListener(MouseEvent.MOUSE_UP, back_btn_up);
			bottomy.back.addEventListener(MouseEvent.MOUSE_UP, back_btn_handler_AfterSearch_up);
		}
		
		private function back_btn_handler_AfterSearch_up(e:Event):void
		{
			bottomy.back.gotoAndStop(1);
			home_btn_up(e);
			
			//Change to original listener
			bottomy.back.addEventListener(MouseEvent.MOUSE_UP, back_btn_up);
			bottomy.back.removeEventListener(MouseEvent.MOUSE_UP, back_btn_handler_AfterSearch_up);
		}
		
		//HOME AND BACK BUTTON EVENT
		private function back_btn_up(e:Event):void
		{
			bottomy.back.gotoAndStop(1);
			
			if(mappy.isRendering())
			{
				mappy.destroy();
			}
			if(catagories!=catagories_root)
			{
				catagories = catagories.parent().parent().children();
				fill_catlist(catagories, listItemSelected_CallBack);
			}
			else
			{
				trace("Back to intro screen");
			}
		}
		
		private function home_btn_up(e:Event):void
		{
			bottomy.home.gotoAndStop(1);
			
			if(mappy.isRendering())
			{
				mappy.destroy();
			}
			if(catagories!=catagories_root)
			{
				catagories=catagories_root;
				fill_catlist(catagories, listItemSelected_CallBack);
			}
			else
			{
				trace("HelpScreen?");
			}
		}
		
		private function home_btn_down(e:Event):void
		{
			bottomy.home.gotoAndStop(2);
		}
		
		private function back_btn_down(e:Event):void
		{
			bottomy.back.gotoAndStop(2);
		}
		
		private function listItemSelected_CallBack(e:Event):void
		{
			catagories = xml_util.searchChildren(e.target.textfield.text, catagories);
			
			var count:uint = 0;
			for(var j:Number = 0; j<catagories.length(); j++)
			{
				if(skeen_utils.clearString(catagories[j].name) != "")
					count++;
			}
			
			if(count == 0)
			{
				var search_result = xml_util.searchCatagory(e.target.textfield.text, sites);
				fill_datalist(search_result);
				
				mappy.seedNodes(search_result);
				if(!mappy.isRendering())
				{
					mappy.render();
				}
				return;
			}
			fill_catlist(catagories, listItemSelected_CallBack);
		}
		
		private function dataItemSelected_CallBack(e:Event):void
		{
			trace("dataItemSelected");
		}
		
		private function searchItemSelected_CallBack(e:Event):void
		{
			trace("searchItemSelected");
		}
		
		//SEARCH FIX PLOX
	}
}
