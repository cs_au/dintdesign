﻿package skeen
{
	// Google Maps Stuff
	import com.google.maps.*;
	import com.google.maps.styles.*;
	import com.google.maps.overlays.*;
	
	// Flash Stuff
	import flash.events.Event;
	import flash.geom.Point;
	import flash.display.MovieClip;
	import flash.net.URLRequest;
	import flash.net.URLLoader;
	import flash.net.URLRequestMethod;
	import flash.net.URLLoaderDataFormat;
	
	public class map extends MovieClip
	{
		// Google map
		private var gmap:Map = null;
		private var nodes:XMLList = null;
		private var isReady:Boolean = false;
		private var rendering:Boolean = false;
		private var coords:Array = [];
		
		public function isRendering():Boolean
		{
			return rendering;
		}
		
		public function map() 
		{
			gmap = new Map();
			gmap.key = "ABQIAAAApPYjsmnEUjM3pRaSdC7R6hRu_Xz-kySXqif81RjcZBsDA50USRSjISfF9SMFFi0VlOTfyXiaUDU5Bw";
			gmap.sensor = "true";
			gmap.setSize(new Point(340, 274));
			gmap.x = 0;
			gmap.y = 65;
			
			gmap.addEventListener(MapEvent.MAP_READY, onMapReady);
		}
		
		public function seedNodes(init_nodes:XMLList)
		{
			nodes = init_nodes;
			coords = [];
			if(isReady)
			{
				loadNodes();
			}
		}
		
		public function render():void
		{
			addChild(gmap);
			rendering=true;
		}
		
		public function destroy():void
		{
			removeChild(gmap);
			gmap.clearOverlays();
			nodes=null;
			coords=[];
			rendering=false;
		}
		
		public function setSize(x:uint, y:uint):void
		{
			gmap.setSize(new Point(x, y));
		}
		
		public function setPos(x:uint, y:uint):void
		{
			gmap.x = x;
			gmap.y = y;
		}
		
		private function onMapReady(event:Event):void
		{
			if(nodes!=null)
			{
				loadNodes();
			}
			isReady=true;
		}
		
		private function loadNodes():void
		{
            var node:XML
            for each(node in nodes)
			{
				buildMarker(node);
			}
		}

		private function buildMarker(node:XML):void
		{
            startAsyncGeoCoding(node.address);
		}
        
		private function geoLoading_complete(evt:Event):void
		{
			//Det ville være nemt at tilføje useren, blot ved at give dem et latlng, og så indludere dette
        	var res:XML = new XML(evt.target.data);
			var coord:LatLng = new LatLng(res.result.geometry.location.lat,
										  res.result.geometry.location.lng);
			coords.push(coord);
			
			var config:MarkerOptions = new MarkerOptions
			({
				strokeStyle: new StrokeStyle({color: 0x987654}),
                fillStyle: new FillStyle({color: 0x223344, alpha: 0.8}),
                radius: 10,
                hasShadow: true
			});
                
			var marker:Marker = new Marker(coord, config);
			gmap.addOverlay(marker);
			
			center();
        } 
		
        private function startAsyncGeoCoding(address:String):void
        {   
            var urlRequest:URLRequest = new URLRequest(googleGeo(address));
            urlRequest.method = URLRequestMethod.GET;
			
            var geoLoader:URLLoader = new URLLoader();
            geoLoader.dataFormat = URLLoaderDataFormat.TEXT;
            geoLoader.addEventListener(Event.COMPLETE, geoLoading_complete);
            geoLoader.load(urlRequest);
                  
            function googleGeo(data:String):String
            {
                return "http://maps.googleapis.com/maps/api/geocode/xml?sensor="
						+ gmap.sensor + "&address=" + escape(data);
            }
        }
		
		private function center():void
		{
			var maxLng:Number = coords[0].lng();
			var minLng:Number = coords[0].lng();
			var maxLat:Number = coords[0].lat();
			var minLat:Number = coords[0].lat();
			
			for(var i:Number = 0; i<coords.length; i++)
			{
				if(coords[i].lat() > maxLat)
					maxLat = coords[i].lat();
				else if(coords[i].lat() < minLat)
					minLat = coords[i].lat();
					
				if(coords[i].lng() > maxLng)
					maxLng = coords[i].lng();
				else if(coords[i].lng() < minLng)
					minLng = coords[i].lng();
			}
			
			var center_Lat:Number = (maxLat+minLat)/2;
			var center_Lng:Number = (maxLng+minLng)/2;
			
			gmap.setCenter(new LatLng(center_Lat, center_Lng), 1, MapType.NORMAL_MAP_TYPE);
			
			if(minLat==maxLat || minLng == maxLng)
			{
				maxLat=maxLat+0.001;
				maxLng=maxLng+0.001;
			}
			
			var oldBounds:LatLngBounds = new LatLngBounds
			(
    			new LatLng(minLat, minLng),
    			new LatLng(maxLat, maxLng)
			); 
			
			while(1)
			{
				if(!gmap.getLatLngBounds().containsBounds(oldBounds))
				{
					gmap.setZoom(gmap.getZoom() - 2);
					break;
				}
				gmap.setZoom(gmap.getZoom() + 1);
			}
		}
	}
}
