﻿package skeen
{	
	import flash.display.MovieClip;
	import flash.display.Stage;
	import skeen.catagory_engine;
    import flash.events.*;
    import flash.net.*;

	public class engine extends MovieClip
	{
		private var stagy:Stage = null;
		private var bottomHome:bottomBar = null;
		
		public function engine(stagevar_init:Stage)
		{
			stagy = stagevar_init;
		}
		
		public function render():void
		{
			bottomHome = new bottomBar();
			bottomHome.y = 640;
			//bottomHome.back.addEventListener(MouseEvent.MOUSE_DOWN, back_btn_down);
			//bottomHome.back.addEventListener(MouseEvent.MOUSE_UP, back_btn_up);
			bottomHome.back.textfield.text = "Quit";
			//bottomHome.home.addEventListener(MouseEvent.MOUSE_DOWN, home_btn_down);
			//bottomHome.home.addEventListener(MouseEvent.MOUSE_UP, home_btn_up);
			bottomHome.home.textfield.text = "Help";
			stagy.addChild(bottomHome);
			
			var cat_engine:catagory_engine = new catagory_engine(stagy);
			destroy();
			cat_engine.render();
		}
		
		public function destroy():void
		{
			stagy.removeChild(bottomHome);
			//bottomHome.removeEventListener(MouseEvent.MOUSE_UP, home_btn_up);
			//bottomHome.removeEventListener(MouseEvent.MOUSE_DOWN, home_btn_down);
			//bottomHome.removeEventListener(MouseEvent.MOUSE_UP, back_btn_up);
			//bottomHome.removeEventListener(MouseEvent.MOUSE_DOWN, back_btn_down);
			bottomHome = null;
		}
	}
}
