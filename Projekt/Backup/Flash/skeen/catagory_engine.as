﻿package skeen
{
	// Flash packages
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.system.fscommand;
	
	public class catagory_engine extends MovieClip
	{
		// The main list object
		private var cat_list:list = null;
		private var data_list:list = null;
		
		// Searchy
		private var searchy:searchBar = null;
		private var keyboardy:keyboard = null;
		
		// bottomy
		private var bottomCat:bottomBar = null;
		private var bottomHome:bottomBar = null;
		private var bottomSearch:bottomBar = null;
		private var bottomSelected:bottomBar = null;	
		private var bottomInfo:bottomBar = null;
		
		// Google maps 
		private var mappy:map = null;
	
		// Loader stuff
		private var catagories_loader:URLLoader = null;
		private var catagories:XMLList = null;
		private var catagories_root:XMLList = null;
	
		private var sites_loader:URLLoader = null;
		private var sites:XMLList = null;
		
		// Info page
		private var titleBar:TitleBar = null;
		private var descriptionBox:DescriptionBox = null;
		
		// stageVar
		private var stagy:Stage = null;
		
		private static var drawOffset_y:Number = 58;
	
		public function catagory_engine(stagevar_init:Stage)
		{
			stagy = stagevar_init;
			
			catagories_loader = new URLLoader();
			sites_loader = new URLLoader();
			
			// Load XML
			catagories_loader.addEventListener(Event.COMPLETE, function(e:Event)
			{	
				catagories_root = catagories = (new XML(catagories_loader.data)).children();
				fill_catlist(catagories, listItemSelected_CallBack);
			});
			sites_loader.addEventListener(Event.COMPLETE, function(e:Event)
			{	
				sites = (new XML(sites_loader.data)).children();	
			});
			catagories_loader.load(new URLRequest("catagories.xml"));
			sites_loader.load(new URLRequest("sites.xml"));
		}
		
		public function render():void
		{
			keyboardy = new keyboard();
			keyboardy.addEventListener("KeyPressed", keyboard_key_pressed);
			keyboardy.addEventListener("EnterPressed", keyboard_enter_pressed);
			keyboardy.y = 375;
			keyboardy.visible = false;
			stagy.addChild(keyboardy);
			
			searchy = new searchBar();
			searchy.textfield.addEventListener(MouseEvent.CLICK, search_field_clicked);
			stagy.addChild(searchy);
			
			bottomInfo = new bottomBar();
			bottomInfo.y = 640;
			bottomInfo.back.addEventListener(MouseEvent.MOUSE_UP, info_back_event);
			bottomInfo.back.addEventListener(MouseEvent.MOUSE_UP, btn_up);
			bottomInfo.back.addEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomInfo.back.textfield.text = "Back";
			bottomInfo.home.addEventListener(MouseEvent.MOUSE_UP, info_directions_event);
			//bottomInfo.home.addEventListener(MouseEvent.MOUSE_UP, btn_up);
			//bottomInfo.home.addEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomInfo.home.deActivate(); // Deactive as its not implemented
			bottomInfo.home.textfield.text = "Directions";
			bottomInfo.visible = false;
			stagy.addChild(bottomInfo);
			
			bottomSelected = new bottomBar();
			bottomSelected.y = 640;
			bottomSelected.back.addEventListener(MouseEvent.MOUSE_UP, selected_deselect_event);
			bottomSelected.back.addEventListener(MouseEvent.MOUSE_UP, btn_up);
			bottomSelected.back.addEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomSelected.back.textfield.text = "Deselect";
			bottomSelected.home.addEventListener(MouseEvent.MOUSE_UP, selected_info_event);
			bottomSelected.home.addEventListener(MouseEvent.MOUSE_UP, btn_up);
			bottomSelected.home.addEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomSelected.home.textfield.text = "Get Info";
			bottomSelected.visible = false;
			stagy.addChild(bottomSelected);
			
			bottomCat = new bottomBar();
			bottomCat.y = 640;
			bottomCat.back.addEventListener(MouseEvent.MOUSE_UP, cat_back_event);
			bottomCat.back.addEventListener(MouseEvent.MOUSE_UP, btn_up);
			bottomCat.back.addEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomCat.back.textfield.text = "Back";
			bottomCat.home.addEventListener(MouseEvent.MOUSE_UP, cat_home_event);
			bottomCat.home.addEventListener(MouseEvent.MOUSE_UP, btn_up);
			bottomCat.home.addEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomCat.home.textfield.text = "Home";
			bottomCat.visible = false;
			stagy.addChild(bottomCat);
			
			bottomHome = new bottomBar();
			bottomHome.y = 640;
			bottomHome.back.addEventListener(MouseEvent.MOUSE_UP, home_quit_event);
			//bottomHome.back.addEventListener(MouseEvent.MOUSE_UP, btn_up);
			//bottomHome.back.addEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomHome.back.deActivate(); // Deactive as its not implemented
			bottomHome.back.textfield.text = "Quit";
			bottomHome.home.addEventListener(MouseEvent.MOUSE_UP, home_help_event);
			//bottomHome.home.addEventListener(MouseEvent.MOUSE_UP, btn_up);
			//bottomHome.home.addEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomHome.home.deActivate(); // Deactive as its not implemented
			bottomHome.home.textfield.text = "Help";
			bottomHome.visible = true;
			stagy.addChild(bottomHome);
			
			bottomSearch = new bottomBar();
			bottomSearch.y = 640;
			bottomSearch.back.addEventListener(MouseEvent.MOUSE_UP, search_back_event);
			bottomSearch.back.addEventListener(MouseEvent.MOUSE_UP, btn_up);
			bottomSearch.back.addEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomSearch.back.textfield.text = "Back";
			bottomSearch.home.addEventListener(MouseEvent.MOUSE_UP, search_search_event);
			bottomSearch.home.addEventListener(MouseEvent.MOUSE_UP, btn_up);
			bottomSearch.home.addEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomSearch.home.textfield.text = "Search";
			bottomSearch.visible = false;
			stagy.addChild(bottomSearch);
			
			cat_list = new list();
			cat_list.y = drawOffset_y;
			stagy.addChild(cat_list);
			
			data_list = new list();
			data_list.y = 400-drawOffset_y;
			data_list.visible = false;
			stagy.addChild(data_list);
			
			mappy = new map();
			mappy.visible = false;
			stagy.addChild(mappy);
			
			titleBar = new TitleBar();
			titleBar.visible = false;
			stagy.addChild(titleBar);
			
			descriptionBox = new DescriptionBox();
			descriptionBox.visible = false;
			descriptionBox.y = drawOffset_y;
			stagy.addChild(descriptionBox);
		}
		
		private function destroy():void
		{
			stagy.removeChild(descriptionBox);
			descriptionBox = null;
			
			stagy.removeChild(titleBar);
			titleBar = null;
			
			stagy.removeChild(mappy);
			mappy = null;
			
			stagy.removeChild(data_list);
			data_list = null;
			
			stagy.removeChild(cat_list);
			cat_list = null;
			
			stagy.removeChild(bottomInfo);
			bottomInfo.home.removeEventListener(MouseEvent.MOUSE_UP, info_directions_event);
			bottomInfo.home.removeEventListener(MouseEvent.MOUSE_UP, btn_up);
			bottomInfo.home.removeEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomInfo.back.removeEventListener(MouseEvent.MOUSE_UP, info_back_event);
			bottomInfo.back.removeEventListener(MouseEvent.MOUSE_UP, btn_up);
			bottomInfo.back.removeEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomInfo = null;
			
			stagy.removeChild(bottomSelected);
			bottomSelected.home.removeEventListener(MouseEvent.MOUSE_UP, selected_info_event);
			bottomSelected.home.removeEventListener(MouseEvent.MOUSE_UP, btn_up);
			bottomSelected.home.removeEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomSelected.back.removeEventListener(MouseEvent.MOUSE_UP, selected_deselect_event);
			bottomSelected.back.removeEventListener(MouseEvent.MOUSE_UP, btn_up);
			bottomSelected.back.removeEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomSelected = null;
			
			stagy.removeChild(bottomSearch);
			bottomSearch.home.removeEventListener(MouseEvent.MOUSE_UP, search_search_event);
			bottomSearch.home.removeEventListener(MouseEvent.MOUSE_UP, btn_up);
			bottomSearch.home.removeEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomSearch.back.removeEventListener(MouseEvent.MOUSE_UP, search_back_event);
			bottomSearch.back.removeEventListener(MouseEvent.MOUSE_UP, btn_up);
			bottomSearch.back.removeEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomSearch = null;
			
			stagy.removeChild(bottomHome);
			bottomHome.home.removeEventListener(MouseEvent.MOUSE_UP, home_quit_event);
			//bottomHome.home.removeEventListener(MouseEvent.MOUSE_UP, btn_up);
			//bottomHome.home.removeEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomHome.back.removeEventListener(MouseEvent.MOUSE_UP, home_help_event);
			//bottomHome.back.removeEventListener(MouseEvent.MOUSE_UP, btn_up);
			//bottomHome.back.removeEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomHome = null;
			
			stagy.removeChild(bottomCat);
			bottomCat.home.removeEventListener(MouseEvent.MOUSE_UP, cat_home_event);
			bottomCat.home.removeEventListener(MouseEvent.MOUSE_UP, btn_up);
			bottomCat.home.removeEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomCat.back.removeEventListener(MouseEvent.MOUSE_UP, cat_back_event);
			bottomCat.back.removeEventListener(MouseEvent.MOUSE_UP, btn_up);
			bottomCat.back.removeEventListener(MouseEvent.MOUSE_DOWN, btn_down);
			bottomCat = null;
			
			stagy.removeChild(searchy);
			searchy.textfield.removeEventListener(MouseEvent.CLICK, search_field_clicked);
			searchy = null;
			
			stagy.removeChild(keyboardy);		
			keyboardy.addEventListener(Event.UNLOAD, keyboard_enter_pressed);
			keyboardy.addEventListener(Event.ACTIVATE, keyboard_key_pressed);
			keyboardy = null;
		}
		
		private function fill_catlist(data:XMLList, func:Function):void
		{
			cat_list.visible = true;
			data_list.visible = false;
			cat_list.seedCallback(func);
			cat_list.removeAll();
			for(var j:Number = 0; j<data.length(); j++)
			{
				if(skeen_utils.clearString(data[j].name) == "")
					continue;
				cat_list.add(data[j].name, XML(data[j]), data[j].icon);
			}
		}
		
		private function fill_datalist(data:XMLList):void
		{
			cat_list.visible = false;
			data_list.visible = true;
			data_list.seedCallback(dataItemSelected_CallBack);
			data_list.removeAll();
			for(var j:Number = 0; j<data.length(); j++)
			{
				data_list.add(data[j].name, XML(data[j]), data[j].icon);
			}
		}
		
		private function search_field_clicked(e:Event):void
		{
			if(searchy.textfield.text!="Search...")
				keyboardy.text = searchy.textfield.text;
				
			var search_result:XMLList = xml_util.search(searchy.textfield.text, sites);
			fill_catlist(search_result, searchItemSelected_CallBack);
			
			catagories=catagories_root;
			
			mappy.visible = false;
			keyboardy.visible = true;
			bottomSearch.visible = true;
			bottomHome.visible = false;
			bottomCat.visible = false;
			bottomSelected.visible = false;
		}
		
		private function keyboard_key_pressed(e:Event):void
		{
			searchy.textfield.text = e.currentTarget.text;
			
			var search_result:XMLList = xml_util.search(searchy.textfield.text, sites);
			fill_catlist(search_result, searchItemSelected_CallBack);
		}
		
		private function keyboard_enter_pressed(e:Event):void
		{
			search_search_event(e);
		}
		
		private function cat_back_event(e:Event):void
		{
			mappy.visible = false;
	
			if(catagories==catagories_root)
			{
				search_field_clicked(e);
				return;
			}
			else
			{
				catagories = catagories.parent().parent().children();
				fill_catlist(catagories, listItemSelected_CallBack);
			}
	
			if(catagories==catagories_root)
			{
				bottomSearch.visible = false;
				bottomHome.visible = true;
				bottomCat.visible = false;
				bottomSelected.visible = false;
			}
		}
		
		private function cat_home_event(e:Event):void
		{
			if(catagories!=catagories_root || data_list.visible==true)
			{
				catagories=catagories_root;
				fill_catlist(catagories, listItemSelected_CallBack);
				
				bottomSearch.visible = false;
				bottomHome.visible = true;
				bottomCat.visible = false;
				mappy.visible = false;
				bottomSelected.visible = false;
			}
		}
		
		private function home_quit_event(e:Event):void
		{
			trace("home_quit_event - Not currently Implemented");
		}
		
		private function home_help_event(e:Event):void
		{
			trace("home_help_event - Not currently Implemented");
		}
		
		private function info_directions_event(e:Event):void
		{
			trace("info_directions_event - Not currently Implemented");
		}
		
		private function search_back_event(e:Event):void
		{
			keyboardy.visible = false;
			bottomSearch.visible = false;
			bottomHome.visible = true;
			bottomCat.visible = false;
			bottomSelected.visible = false;
			
			catagories=catagories_root;
			fill_catlist(catagories, listItemSelected_CallBack);
		}
		
		private function search_search_event(e:Event):void
		{
			var search_result:XMLList = xml_util.search(searchy.textfield.text, sites);
			fill_datalist(search_result);
			
			mappy.seedNodes(search_result);
			mappy.visible = true;
			
			keyboardy.visible = false;
			bottomSearch.visible = false;
			bottomHome.visible = false;
			bottomCat.visible = true;
			bottomSelected.visible = false;
		}
		
		private function selected_deselect_event(e:Event):void
		{
			data_list.deselectAll();
			
			bottomSearch.visible = false;
			bottomHome.visible = false;
			bottomCat.visible = true;
			bottomSelected.visible = false;
		}
		
		private function selected_info_event(e:Event):void
		{
			var data:XML = data_list.getCurrentlySelected();
			
			mappy.visible = false;
			data_list.visible = false;
			
			bottomSelected.visible = false;
			bottomInfo.visible = true;
			
			titleBar.visible = true;
			titleBar.textfield.text = data.name;
			
			descriptionBox.visible = true;
			descriptionBox.address.text = data.address;
			descriptionBox.phone.text = data.phone;
			descriptionBox.website.text = data.website;
			descriptionBox.hours.text = data.openingHours;
			descriptionBox.description.text = data.description;
		}
		
		private function info_back_event(e:Event):void
		{
			descriptionBox.visible = false;
			titleBar.visible = false;
			
			bottomInfo.visible = false;
			bottomSelected.visible = true;
			
			data_list.visible = true;
			mappy.visible = true;
		}
		
		private function btn_down(e:Event):void
		{
			e.currentTarget.gotoAndStop(2);
		}
		
		private function btn_up(e:Event):void
		{
			e.currentTarget.gotoAndStop(1);
		}
		
		private function listItemSelected_CallBack(e:Event):void
		{
			keyboardy.visible = false;
			bottomSearch.visible = false;
			bottomHome.visible = false;
			bottomCat.visible = true;
			bottomSelected.visible = false;
			
			catagories = xml_util.searchChildren(e.target.textfield.text, catagories);
			
			var count:uint = 0;
			for(var j:Number = 0; j<catagories.length(); j++)
			{
				if(skeen_utils.clearString(catagories[j].name) != "")
					count++;
			}
			if(count == 0)
			{
				var search_result = xml_util.searchCatagory(e.target.textfield.text, sites);
				fill_datalist(search_result);
				
				mappy.seedNodes(search_result);
				mappy.visible = true;
				return;
			}
			else
			{
				fill_catlist(catagories, listItemSelected_CallBack);
			}
		}
		
		private function dataItemSelected_CallBack(e:Event):void
		{
			var found:Boolean = data_list.fadeSelected((listItem)(e.currentTarget));
			if(found)
			{
				bottomSearch.visible = false;
				bottomHome.visible = false;
				bottomCat.visible = false;
				bottomSelected.visible = true;
			}
		}
		
		private function searchItemSelected_CallBack(e:Event):void
		{
			trace("searchItemSelected - Not currently Implemented");
		}
	}
}
