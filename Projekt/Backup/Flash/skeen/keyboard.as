﻿package skeen
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.events.Event;

	public class keyboard extends MovieClip
	{
		// keyboard
		private var row1:Array = new Array("1","2","3","4","5","6","7","8","9","0","<");
		private var row2:Array = new Array("Q","W","E","R","T","Y","U","I","O","P","Å");
		private var row3:Array = new Array("A","S","D","F","G","H","J","K","L","Æ","Ø");
		private var row4:Array = new Array("Z","X","C","V","B","N","M",",",".","-","↵");

		public var text:String = new String();

		public function keyboard()
		{
			for (var i1:Number = 0; i1 < row1.length; i1++)
			{
				var key1:key = new key();
				key1.addEventListener(MouseEvent.MOUSE_UP, addToField);
				key1.addEventListener(MouseEvent.MOUSE_DOWN, keyDown);
				key1.textfield.text = row1[i1];
				this.addChild(key1);
				key1.x = 5 + i1 * 30;
				key1.y = 2;
			}

			for (var i2:Number = 0; i2 < row2.length; i2++)
			{
				var key2:key = new key();
				key2.addEventListener(MouseEvent.MOUSE_UP, addToField);
				key2.addEventListener(MouseEvent.MOUSE_DOWN, keyDown);
				key2.textfield.text = row2[i2];
				this.addChild(key2);
				key2.x = 5 + i2 * 30;
				key2.y = 42;
			}

			for (var i3:Number = 0; i3 < row3.length; i3++)
			{
				var key3:key = new key();
				key3.addEventListener(MouseEvent.MOUSE_UP, addToField);
				key3.addEventListener(MouseEvent.MOUSE_DOWN, keyDown);
				key3.textfield.text = row3[i3];
				this.addChild(key3);
				key3.x = 5 + i3 * 30;
				key3.y = 84;
			}

			for (var i4:Number = 0; i4 < row4.length; i4++)
			{
				var key4:key = new key();
				key4.addEventListener(MouseEvent.MOUSE_UP, addToField);
				key4.addEventListener(MouseEvent.MOUSE_DOWN, keyDown);
				key4.textfield.text = row4[i4];
				this.addChild(key4);
				key4.x = 5 + i4 * 30;
				key4.y = 126;
			}
			
			var space:keySpace = new keySpace();
			space.addEventListener(MouseEvent.MOUSE_UP, addToField);
			space.addEventListener(MouseEvent.MOUSE_DOWN, keyDown);
			space.textfield.text = " ";
			this.addChild(space);
			space.x = 340/2 - space.width/2;
			space.y = 168;
		}
		
		private function addToField(e:Event):void
		{
			e.currentTarget.gotoAndStop(1);
			if(e.currentTarget.textfield.text == "<")
			{
				text = text.slice(0, text.length-1);
			}
			else if(e.currentTarget.textfield.text == "↵")
			{
				dispatchEvent(new Event("EnterPressed"));
				return;
			}
			else
			{
				text = text.concat(e.currentTarget.textfield.text);
			}
			dispatchEvent(new Event("KeyPressed"));
		}
		
		private function keyDown(e:Event):void
		{
			e.currentTarget.gotoAndStop(2);
		}
	}
}