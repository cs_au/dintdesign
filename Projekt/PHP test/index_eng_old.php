<html>
	<head>
		<title>Smartphone App Sandbox</title>
		<style type="text/css">
			body { font-family: sans-serif; }
			#app { position: relative; top: 164px; left: 53px; }
			.description { width: 500px; }
			.appback { background-image: url("nokiascale.png"); background-repeat: no-repeat; vertical-align: top; }
		</style>
	</head>
	<body>
		<table width="100%" height="871px">
			<tr>
				<td width="20px"></td>
				<td class="description">
<h3>About this test</h3>
<p>The following usability test is designed around finding flaws, as well as analyzing the amount of "screen touches" (mouse clicks) typically needed to perform a series of "tasks".</p>

<p>The prototype is a smartphone app which is meant to be used as a "mobile tourist information office". Imagine being a tourist in the city of �rhus, and that you must use this app to find information about tourist attractions, hotels, restaurants, etc.</p>

<p>By "information" we mean descriptions of places/sites, addresses, phone numbers, opening hours, etc.</p>

<p>When you have completed a task, press the "Done" button (at the bottom), then continue on to the next task (if you can't find your way back to the "Home screen", you can refresh this page). A task is considered completed when no more clicks are required to see the inquired information.</p>

<p>Thank you for your time :)</p>

<p>NOTE: Nothing bad happens should you make a mistake - we're testing the app, not you.</p>

<h3>Tasks</h3>
<p><b>Task #1</b>: Find the address of the ARoS museum. (Note that you HAVE to click the Search-button because the search list itself is not implemented yet)</p>

<p><b>Task #2</b>: Find a phone number of a hotel of your choice.</p>

<p><b>Task #3</b>: Use the Search feature to find all nearby Burger King restaurants.</p>

<p><b>Task #4</b>: Use the Browsing feature to find a place to buy some clothes.</p>

<p><a href="comments_eng.php">Click here</a> when you're done with all 4 tasks.<p>
				</td>
				<td width="20px"></td>
				<td class="appback">
					<div id="app">
						<object
						  classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
						  codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0"
						  width="289px"
						  height="544px">
						  <param name="movie" value="App.swf">
						  <param name="quality" value="high">
						  <param name="flashVars" value="key=ABQIAAAAGcscEW_-mn3EKWpdbpYswRS7G7cziiCHRzORmwLZhFBuoLLnvhRIWrQpdixscmO51Iy7PRBgaaewGA&sensor=true">
						  <embed
							width="289px"
							height="544px"
							src="App.swf"
							quality="high"
							flashVars="key=ABQIAAAAGcscEW_-mn3EKWpdbpYswRS7G7cziiCHRzORmwLZhFBuoLLnvhRIWrQpdixscmO51Iy7PRBgaaewGA&sensor=true"
							pluginspage="http://www.macromedia.com/go/getflashplayer"
							type="application/x-shockwave-flash">
						  </embed>
						</object>
					</div>
				</td>
			</tr>
		</table>
	</body>
</html>