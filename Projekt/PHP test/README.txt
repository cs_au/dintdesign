Der er problemer med at k�re app'en standalone,
s� man SKAL teste den fra Flash. Upload til
000webhost (andreas.netne.net) virker ikke, da
der er problemer med XML-filer p� deres servers.
Upload til myplus (duck.myplus.org) virker ikke
pga. app'ens ustabilitet som standalone swf.

F�lgende filer er n�dvendige for at MySQL-stads
virker til data-opsamling:

---------- test.php ----------

Extracter al data fra MySQL-databasen p� formen

Array
(
	[i] => [i] <STEP>
)

hvor i er antallet af klik indtil videre, og
<STEP> er det element der er trykket p�

---------- array_test.php ----------

Bruges til at sende data til MySQL-databasen ved
at afl�se POST-requests.



Om testen:

F�lgende usability test er udarbejdet med fokus
p� at spotte fejl og mangler, samt analysere
antal "sk�rmber�ringer" (museklik) der typisk
bruges til en r�kke "tasks".

Prototypen er en smartphone app, der bruges som
et "mobilt turistkontor". Forestil dig, at du er
turist i �rhus, og at du udelukkende skal bruge
app'en til at finde information om attraktioner,
hoteller, restauranter og lignende.

Med "information" menes der sted-beskrivelser,
adresser, telefonnumre, �bningstider, etc.

N�r du har udf�rt en task trykker du p� "Done"
knappen og forts�tter til n�ste task. En task
opfattes som udf�rt, n�r der ikke kr�ves flere
klik for at se den information der sp�rges til.

P� forh�nd tak for hj�lpen :)

NOTE: Der sker intet hvis du laver fejl - det er
app'en vi tester - ikke dig.

Task #1: Find adressen p� Aros-museet.

Task #2: Find et telefonnummer til et valgfrit
hotel.

Task #3: Brug s�gefunktionen til at finde alle
n�rliggende McDonald's restauranter.

Task #4: Brug kategori-browsing til at finde en
butik at k�be noget smart hip-hop t�j i.

