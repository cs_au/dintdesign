F�lgende tilf�jelser skal laves i engine.as for
at f� MySQL-stads til at virke:

public class engine extends MovieClip
{
	...
	public var clickCounter:Number = 0;
	public var clickString:String = "";
	...
}

public function clickHandler(e:MouseEvent):void
{
	clickCounter++;
	
	if (e.target.parent.parent.name == "textfield")
		clickString += "|[" + clickCounter + "] " + e.target.parent.parent.text;
	else if (e.target.name == "icon")
		clickString += "|[" + clickCounter + "] " + e.target.parent.getChildAt(1).text;
	else
		clickString += "|[" + clickCounter + "] " + e.target.name;
	
	trace(clickString);
}

public function engine(stagevar_init:Stage)
{
	...
	stagy.addEventListener(MouseEvent.MOUSE_DOWN, clickHandler);
	...
}








F�lgende skal inds�ttes et eller andet sted,
m�ske engine.as eller category_engine.as.
"sendData" er et normalt movieclip:

sendData.addEventListener(MouseEvent.CLICK, sendDataEvent);

function sendDataEvent(e:MouseEvent):void
{
	var requesty:URLRequest = new URLRequest("http://duck.myplus.org/dIntDesign/array_test.php");
	requesty.method = URLRequestMethod.POST;
	
	var variables:URLVariables = new URLVariables();
	
	variables.datastring = main.clickString;
	requesty.data = variables; 
	
	var loader:URLLoader = new URLLoader();
	loader.dataFormat = URLLoaderDataFormat.VARIABLES;
	loader.addEventListener(Event.COMPLETE, doComplete);
	loader.load(requesty);
	
	main.clickCounter = 0;
	main.clickString = "";
}
	
function doComplete (event:Event):void
{
	trace(event.target.data);
}
