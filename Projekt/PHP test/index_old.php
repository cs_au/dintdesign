<html>
	<head>
		<title>Smartphone App Sandbox</title>
		<style type="text/css">
			body { font-family: sans-serif; }
			#app { position: relative; top: 164px; left: 53px; }
			.description { width: 500px; }
			.appback { background-image: url("nokiascale.png"); background-repeat: no-repeat; vertical-align: top; }
		</style>
	</head>
	<body>
		<table width="100%" height="871px">
			<tr>
				<td width="20px"></td>
				<td class="description">
<h3>Om testen</h3>

<p>F�lgende usability test er udarbejdet med fokus p� at spotte fejl og mangler, samt analysere antal "sk�rmber�ringer" (museklik) der typisk bruges til en r�kke "tasks".</p>

<p>Prototypen er en smartphone app, der bruges som et "mobilt turistkontor". Forestil dig, at du er turist i �rhus, og at du udelukkende skal bruge app'en til at finde information om attraktioner, hoteller, restauranter og lignende.</p>

<p>Med "information" menes der sted-beskrivelser, adresser, telefonnumre, �bningstider, etc.</p>

<p>N�r du har udf�rt en task trykker du p� "Done" knappen (i bunden), og forts�tter derefter til n�ste task (hvis du ikke kan finde tilbage til "Home screen" kan du refresh'e siden). En task opfattes som udf�rt, n�r der ikke kr�ves flere klik for at se den information der sp�rges til.</p>

<p>P� forh�nd tak for hj�lpen :)</p>

<p>NOTE: Der sker intet hvis du laver fejl - det er app'en vi tester - ikke dig.</p>

<h3>Tasks</h3>
<p><b>Task #1</b>: Find adressen p� ARoS-museet. (Bem�rk at man SKAL trykke p� Search-knappen, da s�gelisten i sig selv ikke er implementeret endnu)</p>

<p><b>Task #2</b>: Find et telefonnummer til et valgfrit hotel.</p>

<p><b>Task #3</b>: Brug s�gefunktionen til at finde alle n�rliggende Burger King restauranter.</p>

<p><b>Task #4</b>: Brug kategori-browsing til at finde en butik at k�be t�j i.</p>

<p><a href="comments.php">Klik her</a> n�r du er f�rdig med alle 4 tasks.<p>
				</td>
				<td width="20px"></td>
				<td class="appback">
					<div id="app">
						<object
						  classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
						  codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0"
						  width="289px"
						  height="544px">
						  <param name="movie" value="App.swf">
						  <param name="quality" value="high">
						  <param name="flashVars" value="key=ABQIAAAAGcscEW_-mn3EKWpdbpYswRS7G7cziiCHRzORmwLZhFBuoLLnvhRIWrQpdixscmO51Iy7PRBgaaewGA&sensor=true">
						  <embed
							width="289px"
							height="544px"
							src="App.swf"
							quality="high"
							flashVars="key=ABQIAAAAGcscEW_-mn3EKWpdbpYswRS7G7cziiCHRzORmwLZhFBuoLLnvhRIWrQpdixscmO51Iy7PRBgaaewGA&sensor=true"
							pluginspage="http://www.macromedia.com/go/getflashplayer"
							type="application/x-shockwave-flash">
						  </embed>
						</object>
					</div>
				</td>
			</tr>
		</table>
	</body>
</html>